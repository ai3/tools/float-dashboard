FROM node:current-bullseye AS assets
ADD . /src
WORKDIR /src
ENV NODE_OPTIONS=--openssl-legacy-provider
RUN npm install && env PROD=1 ./node_modules/.bin/webpack && ls -lR assets

FROM debian:bullseye-slim AS precompress
RUN apt-get -q update && env DEBIAN_FRONTEND=noninteractive apt-get -qy install brotli
COPY --from=assets /src/assets /src/assets
RUN find /src/assets -type f -regextype egrep -regex '.*\.(css|js|svg|html)' -print \
    | xargs brotli -9k
RUN find /src/assets -type f -regextype egrep -regex '.*\.(css|js|svg|html)' -print \
    | xargs gzip -9k

FROM golang:1.21.6 AS build
ADD . /src
WORKDIR /src
COPY --from=precompress /src/assets /src/assets
RUN ls -l /src/assets
RUN go install github.com/go-bindata/go-bindata/go-bindata@latest
RUN go generate ./... && go build -tags netgo -o /server ./server

FROM registry.git.autistici.org/ai3/thirdparty/graphviz-static:master AS dot_static

FROM scratch
COPY --from=build /server /usr/bin/float-dashboard
COPY --from=dot_static /usr/bin/dot /usr/bin/dot
COPY testdata/ /etc/float/

ENTRYPOINT ["/usr/bin/float-dashboard"]
