package main

//go:generate go-bindata --nocompress --pkg main --prefix ../assets/ ../assets/...

import (
	"context"
	"flag"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"path"
	"strings"
	"syscall"
	"time"

	"git.autistici.org/ai3/tools/float-dashboard/schema"
	"git.autistici.org/ai3/tools/float-dashboard/schema/graph"
	"github.com/coreos/go-systemd/v22/daemon"
	assetfs "github.com/elazarl/go-bindata-assetfs"
	"github.com/lpar/gzipped/v2"
)

func getenv(key, defaultValue string) string {
	if v := os.Getenv(key); v != "" {
		return v
	}
	return defaultValue
}

var (
	svcFile      = flag.String("input", getenv("SERVICES_CONFIG", "/etc/float/services.yml"), "services.yml file")
	leadersFile  = flag.String("leaders", getenv("LEADERS_CONFIG", "/etc/float/service_masters.yml"), "leaders file")
	publicDomain = flag.String("domain", getenv("DOMAIN", ""), "public domain")
	addr         = flag.String("addr", getenv("ADDR", ":3000"), "TCP `addr` to listen on")
)

// A simple AssetFS wrapper that provides the Exists() method needed
// by the gzipped.FileSystem interface.
type assetFSWrapper struct {
	*assetfs.AssetFS
}

func (f *assetFSWrapper) Exists(name string) bool {
	// Match what assetfs.Open does, by removing the leading slash.
	_, err := f.AssetFS.Asset(strings.TrimPrefix(name, "/"))
	return err == nil
}

func newAssetFSWrapper() *assetFSWrapper {
	return &assetFSWrapper{
		AssetFS: &assetfs.AssetFS{
			Asset:     Asset,
			AssetDir:  AssetDir,
			AssetInfo: AssetInfo,
		},
	}
}

type cacheResponseWriter struct {
	http.ResponseWriter
}

func newCacheResponseWriter(w http.ResponseWriter) *cacheResponseWriter {
	return &cacheResponseWriter{
		ResponseWriter: w,
	}
}

func (w *cacheResponseWriter) WriteHeader(statusCode int) {
	if statusCode >= 200 && statusCode < 300 {
		hdr := w.ResponseWriter.Header()
		if ct := hdr.Get("Content-Type"); !strings.HasPrefix(ct, "text/html") && !strings.HasPrefix(ct, "application/json") {
			hdr.Set("Cache-Control", fullCache)
		}
	}
	w.ResponseWriter.WriteHeader(statusCode)
}

const (
	fullCache = "max-age=2592000"
)

// Wrap an http.Handler with cache-friendly headers.
func cacheHeaders(wrap http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		wrap.ServeHTTP(newCacheResponseWriter(w), req)
	})
}

// Find the latest timestamp of any of our input files.
func getDataTimestamp() time.Time {
	var latest time.Time
	for _, f := range []string{*svcFile, *leadersFile} {
		if fi, err := os.Stat(f); err == nil {
			if t := fi.ModTime(); t.After(latest) {
				latest = t
			}
		}
	}
	return latest
}

func buildServer(jsonData []byte, jsonStamp time.Time, gr *graph.Graph) (http.Handler, error) {
	mux := http.NewServeMux()

	mux.HandleFunc("/flow/", func(w http.ResponseWriter, req *http.Request) {
		handleGraph(gr, w, req)
	})

	assetsFS := newAssetFSWrapper()
	fs, err := overlayWithServices(assetsFS, jsonData, jsonStamp)
	if err != nil {
		return nil, fmt.Errorf("failed to build services.json data bundle: %w", err)
	}
	fileSrv := gzipped.FileServer(fs)

	mux.HandleFunc("/", func(w http.ResponseWriter, req *http.Request) {
		// Serve index.html for the site root.
		if req.URL.Path == "" || req.URL.Path == "/" {
			req.URL.Path = path.Join(req.URL.Path, "index.html")
		}

		// Force a Content-Type for services.json requests, we
		// can't rely on http.ServeContent mime autodetection
		// for precompressed requests (see
		// https://github.com/lpar/gzipped/issues/18).
		if req.URL.Path == "/services.json" {
			w.Header().Set("Content-Type", "application/json; charset=utf-8")
		}

		fileSrv.ServeHTTP(w, req)
	})

	return cacheHeaders(mux), nil
}

func main() {
	log.SetFlags(0)
	flag.Parse()

	if *publicDomain == "" {
		log.Fatal("Must specify --domain")
	}

	// Load the service definitions and the leaders map.
	svcmap, err := schema.LoadFloatServices(*svcFile, *leadersFile)
	if err != nil {
		log.Fatal(err)
	}

	// Build the static services.json data that we're going to serve
	// to the Javascript application.
	jsonData, err := schema.Convert(svcmap, *publicDomain)
	if err != nil {
		log.Fatalf("can't build services.json: %v", err)
	}

	// Build the service graph so we can dynamically render dot
	// graphs of service dependencies.
	gr := graph.ServiceGraph(svcmap)

	// Build the HTTP server application.
	h, err := buildServer(jsonData, getDataTimestamp(), gr)
	if err != nil {
		log.Fatal(err)
	}

	// Start the HTTP server, and set it up to terminate
	// gracefully on SIGINT/SIGTERM.
	srv := &http.Server{
		Addr:         *addr,
		Handler:      h,
		ReadTimeout:  30 * time.Second,
		WriteTimeout: 30 * time.Second,
		IdleTimeout:  600 * time.Second,
	}

	sigCh := make(chan os.Signal, 1)
	go func() {
		<-sigCh
		// Gracefully terminate for 3 seconds max, then shut
		// down remaining clients.
		log.Printf("signal received, terminating...")
		daemon.SdNotify(false, "STOPPING=1")
		ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
		defer cancel()
		err := srv.Shutdown(ctx)
		if err == context.Canceled {
			err = srv.Close()
		}
		if err != nil {
			log.Printf("error terminating server: %v", err)
		}
	}()
	signal.Notify(sigCh, syscall.SIGINT, syscall.SIGTERM)

	daemon.SdNotify(false, "READY=1")

	log.Printf("starting static server on %s", *addr)
	if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Fatalf("error: %v", err)
	}
}
