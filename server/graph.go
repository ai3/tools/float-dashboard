package main

import (
	"encoding/json"
	"log"
	"net/http"
	"os/exec"
	"strings"

	"git.autistici.org/ai3/tools/float-dashboard/schema/graph"
)

type graphNodeMeta struct {
	Service string `json:"service"`
}

type graphResponse struct {
	SVG      string                    `json:"svg"`
	NodeMeta map[string]*graphNodeMeta `json:"node_meta"`
}

func handleGraph(gr *graph.Graph, w http.ResponseWriter, req *http.Request) {
	// Gather request params.
	service := req.FormValue("service")
	if service == "" {
		http.Error(w, "missing service", http.StatusBadRequest)
		return
	}

	g := gr.Filter(graph.FilterEdgeByService(service))
	dot := g.Render(
		graph.StyleNodeByService(service),
		graph.StyleEdgeByService(service),
	)

	svg, err := toSVG(dot)
	if err != nil {
		log.Printf("rendering error: %v", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	result := graphResponse{
		SVG:      string(svg),
		NodeMeta: buildNodeMeta(g),
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.Header().Set("Cache-Control", "no-store")
	json.NewEncoder(w).Encode(&result)
}

func toSVG(dot string) ([]byte, error) {
	cmd := exec.Command("dot", "-Tsvg")
	cmd.Stdin = strings.NewReader(dot)
	return cmd.Output()
}

func buildNodeMeta(g *graph.Graph) map[string]*graphNodeMeta {
	m := make(map[string]*graphNodeMeta)
	g.Each(func(n *graph.Node) {
		if n.Service != "" {
			m[n.ID()] = &graphNodeMeta{
				Service: n.Service,
			}
		}
	})
	return m
}
