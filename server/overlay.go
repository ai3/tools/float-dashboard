package main

import (
	"bytes"
	"compress/gzip"
	"net/http"
	"time"

	"github.com/andybalholm/brotli"
	assetfs "github.com/elazarl/go-bindata-assetfs"
	"github.com/lpar/gzipped/v2"
)

// An overlay FileSystem that serves a runtime-generated services.json
// file (with alternate precompressed representations).
//
// We re-use the assetfs.AssetFile type since it already provides a
// nice in-memory http.File implementation.
type overlayFS struct {
	gzipped.FileSystem

	files map[string]*assetfs.AssetFile
}

func newOverlayFS(wrap gzipped.FileSystem) *overlayFS {
	return &overlayFS{
		FileSystem: wrap,
		files:      make(map[string]*assetfs.AssetFile),
	}
}

func (o *overlayFS) addFile(name string, content []byte, mtime time.Time) {
	o.files[name] = assetfs.NewAssetFile(name, content, mtime)
}

func (o *overlayFS) Exists(name string) bool {
	if _, ok := o.files[name]; ok {
		return true
	}
	return o.FileSystem.Exists(name)
}

func (o *overlayFS) Open(name string) (http.File, error) {
	if f, ok := o.files[name]; ok {
		return f, nil
	}
	return o.FileSystem.Open(name)
}

// Create a FileSystem that overlays /services.json (along with
// pre-compressed variations) onto a base FileSysem.
func overlayWithServices(fs gzipped.FileSystem, servicesJSON []byte, stamp time.Time) (gzipped.FileSystem, error) {
	servicesBr, err := compressBr(servicesJSON)
	if err != nil {
		return nil, err
	}

	servicesGz, err := compressGz(servicesJSON)
	if err != nil {
		return nil, err
	}

	// Remember that gzipped.FileServer adds a leading slash to
	// all requested paths.
	o := newOverlayFS(fs)
	o.addFile("/services.json", servicesJSON, stamp)
	o.addFile("/services.json.gz", servicesGz, stamp)
	o.addFile("/services.json.br", servicesBr, stamp)
	return o, nil
}

// Compress a buffer with Brotli
func compressBr(data []byte) ([]byte, error) {
	var buf bytes.Buffer
	w := brotli.NewWriterLevel(&buf, brotli.BestCompression)
	if _, err := w.Write(data); err != nil {
		return nil, err
	}
	if err := w.Flush(); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}

// Compress a buffer with gzip.
func compressGz(data []byte) ([]byte, error) {
	var buf bytes.Buffer
	w, err := gzip.NewWriterLevel(&buf, gzip.BestCompression)
	if err != nil {
		return nil, err
	}
	if _, err := w.Write(data); err != nil {
		return nil, err
	}
	if err := w.Flush(); err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}
