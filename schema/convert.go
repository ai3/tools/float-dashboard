package schema

import (
	"encoding/json"
	"fmt"
	"os"

	dashschema "git.autistici.org/ai3/tools/float-dashboard/schema/dash"
	floatschema "git.autistici.org/ai3/tools/float-dashboard/schema/float"
	"gopkg.in/yaml.v3"
)

func loadYAML(path string, obj interface{}) error {
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()

	return yaml.NewDecoder(f).Decode(obj)
}

func LoadFloatServices(svcFile, leadersFile string) (map[string]*floatschema.Service, error) {
	// Load the service definitions and the leaders map.
	var svcmap map[string]*floatschema.Service
	if err := loadYAML(svcFile, &svcmap); err != nil {
		return nil, err
	}

	var leaders map[string]string
	if err := loadYAML(leadersFile, &leaders); err != nil {
		return nil, err
	}

	// Map back leaders into services.
	for svcname, leader := range leaders {
		if _, ok := svcmap[svcname]; !ok {
			return nil, fmt.Errorf("leaders file references service '%s' which does not exist!", svcname)
		}
		svcmap[svcname].MasterHost = leader
	}

	return svcmap, nil
}

func Convert(svcmap map[string]*floatschema.Service, publicDomain string) ([]byte, error) {
	out := make(map[string]*dashschema.Service)
	for name, fsvc := range svcmap {
		out[name] = dashschema.FromFloat(fsvc, publicDomain)
	}
	return json.Marshal(out)
}
