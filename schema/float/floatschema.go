package floatschema

import (
	"fmt"
)

type MonitoringEndpoint struct {
	JobName string `yaml:"job_name"`
	Port    int    `yaml:"port"`
	Scheme  string `yaml:"scheme,omitempty"`
}

type PublicEndpoint struct {
	Name           string   `yaml:"name"`
	Domains        []string `yaml:"domains,omitempty"`
	Port           int      `yaml:"port"`
	Scheme         string   `yaml:"scheme,omitempty"`
	EnableSSOProxy bool     `yaml:"enable_sso_proxy,omitempty"`
	Sharded        bool     `yaml:"sharded"`
}

// Boolean fields that default to true are problematic, so we use
// pointers to detect the case where the value is unset.
type ServiceCredentials struct {
	Name         string `yaml:"name"`
	EnableClient *bool  `yaml:"enable_client,omitempty"`
	EnableServer *bool  `yaml:"enable_server,omitempty"`
}

func (c *ServiceCredentials) HasClient() bool {
	if c.EnableClient == nil {
		return true
	}
	return *c.EnableClient
}

func (c *ServiceCredentials) HasServer() bool {
	if c.EnableServer == nil {
		return true
	}
	return *c.EnableServer
}

type Container struct {
	Name    string              `yaml:"name"`
	Image   string              `yaml:"image"`
	Port    IntOrStr            `yaml:"port,omitempty"`
	Ports   []IntOrStr          `yaml:"ports,omitempty"`
	Volumes []map[string]string `yaml:"volumes,omitempty"`
	Env     map[string]string   `yaml:"env,omitempty"`
	Args    string              `yaml:"args,omitempty"`
	Root    bool                `yaml:"root,omitempty"`
}

type Dataset struct {
	Name string `yaml:"name"`
	Path string `yaml:"path"`
}

type Service struct {
	Name       string
	Hosts      []string
	MasterHost string

	User                  string               `yaml:"user"`
	NumInstances          string               `yaml:"num_instances,omitempty"`
	SchedulingGroup       string               `yaml:"scheduling_group"`
	MasterElection        bool                 `yaml:"master_election,omitempty"`
	MasterSchedulingGroup string               `yaml:"master_scheduling_group,omitempty"`
	Ports                 []IntOrStr           `yaml:"ports,omitempty"`
	ServiceCredentials    []ServiceCredentials `yaml:"service_credentials,omitempty"`
	MonitoringEndpoints   []MonitoringEndpoint `yaml:"monitoring_endpoints,omitempty"`
	PublicEndpoints       []PublicEndpoint     `yaml:"public_endpoints,omitempty"`
	PublicTCPEndpoints    []PublicEndpoint     `yaml:"public_tcp_endpoints,omitempty"`
	Containers            []Container          `yaml:"containers,omitempty"`
	SystemdServices       []string             `yaml:"systemd_services,omitempty"`
	Datasets              []Dataset            `yaml:"datasets,omitempty"`

	Annotations struct {
		Dependencies []struct {
			Client string `yaml:"client"`
			Server string `yaml:"server"`
		} `yaml:"dependencies"`
	} `yaml:"annotations,omitempty"`
}

type TaggedPort struct {
	Port int
	Tags []string
}

func (s *Service) AllPorts() []TaggedPort {
	tmp := make(map[int]*TaggedPort)
	addTag := func(port int, tag string) {
		tp, ok := tmp[port]
		if !ok {
			tp = &TaggedPort{Port: port}
			tmp[port] = tp
		}
		if tag != "" {
			tp.Tags = append(tp.Tags, tag)
		}
	}

	for _, port := range s.Ports {
		addTag(int(port), "")
	}
	for _, m := range s.MonitoringEndpoints {
		addTag(m.Port, "monitoring")
	}
	for _, p := range s.PublicEndpoints {
		addTag(p.Port, fmt.Sprintf("public:%s", p.Name))
	}
	for _, c := range s.Containers {
		addTag(int(c.Port), fmt.Sprintf("docker:%s", c.Name))
	}

	out := make([]TaggedPort, 0, len(tmp))
	for _, tp := range tmp {
		out = append(out, *tp)
	}
	return out
}
