package floatschema

import (
	"testing"

	"gopkg.in/yaml.v3"
)

func TestIntOrStr(t *testing.T) {
	for _, td := range []struct {
		s     string
		ok    bool
		value int
	}{
		{"\"42\"", true, 42},
		{"42", true, 42},
		{"foo", false, 0},
		{"\"foo\"", false, 0},
	} {
		var i IntOrStr
		err := yaml.Unmarshal([]byte(td.s), &i)
		ok := err == nil

		if ok != td.ok {
			t.Errorf("unmarshaling '%s' got %v, expected %v", td.s, ok, td.ok)
			continue
		}
		if int(i) != td.value {
			t.Errorf("unmarshaling '%s' returned %d, expected %d", td.s, i, td.value)
		}
	}
}
