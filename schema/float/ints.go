package floatschema

import (
	"strconv"

	"gopkg.in/yaml.v3"
)

type IntOrStr int

func (i *IntOrStr) UnmarshalYAML(value *yaml.Node) error {
	var tmpI int
	if err := value.Decode(&tmpI); err == nil {
		*i = IntOrStr(tmpI)
		return nil
	}

	var tmpS string
	if err := value.Decode(&tmpS); err != nil {
		return err
	}
	ii, err := strconv.Atoi(tmpS)
	if err != nil {
		return err
	}
	*i = IntOrStr(ii)
	return nil
}

func IntOrStrListToIntList(l []IntOrStr) []int {
	o := make([]int, 0, len(l))
	for _, i := range l {
		o = append(o, int(i))
	}
	return o
}
