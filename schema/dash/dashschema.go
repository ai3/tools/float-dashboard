package dashschema

import (
	"fmt"
	"strconv"
	"strings"

	floatschema "git.autistici.org/ai3/tools/float-dashboard/schema/float"
)

const (
	LinkRelSelf      = "self"
	LinkRelDashboard = "grafana"
	LinkRelLogs      = "logs"
	LinkRelOther     = "other"
)

type Link struct {
	Name string `json:"name"`
	Rel  string `json:"rel"`
	URL  string `json:"url"`
}

type Process struct {
	DisplayName     string `json:"display_name"`
	SystemdUnitName string `json:"systemd_unit_name"`
	ProgramName     string `json:"program_name"`

	IsContainer       bool   `json:"is_container"`
	HasRootPrivileges bool   `json:"has_root_privileges"`
	Image             string `json:"image"`
	Args              string `json:"args"`

	Ports []int `json:"ports"`

	Links []Link `json:"links"`
}

type Endpoint struct {
	Port          int      `json:"port"`
	Type          string   `json:"type"`
	DNSNames      []string `json:"dns_names,omitempty"`
	ContainerName string   `json:"container_name"`

	Links []Link `json:"links"`
}

const (
	EndpointTypeMonitoring = "monitoring"
	EndpointTypePublic     = "public_http"
	EndpointTypePublicTCP  = "public_tcp"
	EndpointTypeInternal   = "internal"
)

type Assignment struct {
	Host     string `json:"host"`
	IsLeader bool   `json:"is_leader,omitempty"`
}

type Tags map[string][]string

func (t Tags) Add(key, value string) {
	t[key] = append(t[key], value)
}

type Service struct {
	Name string `json:"name"`
	User string `json:"user"`

	SchedulingGroup string       `json:"scheduling_group"`
	NumInstances    int          `json:"num_instances"`
	OnAllHosts      bool         `json:"on_all_hosts"`
	HasLeader       bool         `json:"has_leader"`
	Hosts           []Assignment `json:"hosts"`

	Processes []Process  `json:"processes"`
	Endpoints []Endpoint `json:"endpoints"`

	Links      []Link `json:"links"`
	SearchTags Tags   `json:"search_tags"`
}

func (s *Service) collectSearchTags() {
	tags := make(Tags)

	tags.Add("name", s.Name)
	if s.SchedulingGroup != "all" {
		tags.Add("group", s.SchedulingGroup)
	}
	for _, h := range s.Hosts {
		tags.Add("host", h.Host)
	}
	for _, p := range s.Processes {
		tags.Add("prog", p.ProgramName)
		if p.IsContainer {
			tags.Add("image", p.Image)
		}
	}
	for _, e := range s.Endpoints {
		tags.Add("port", strconv.Itoa(e.Port))
		for _, dnsname := range e.DNSNames {
			tags.Add("dns", dnsname)
		}
	}

	s.SearchTags = tags
}

func FromFloat(fs *floatschema.Service, publicDomain string) *Service {
	s := Service{
		Name:            fs.Name,
		User:            fs.User,
		SchedulingGroup: fs.SchedulingGroup,
		HasLeader:       fs.MasterElection,
	}
	if s.SchedulingGroup == "" {
		s.SchedulingGroup = "all"
	}

	if fs.NumInstances == "all" || fs.NumInstances == "" {
		s.OnAllHosts = true
	} else {
		s.NumInstances, _ = strconv.Atoi(fs.NumInstances)
	}

	for _, h := range fs.Hosts {
		s.Hosts = append(s.Hosts, Assignment{
			Host:     h,
			IsLeader: h == fs.MasterHost,
		})
	}

	containersByPort := make(map[int]string)
	seenPorts := make(map[int]struct{})

	for _, c := range fs.Containers {
		proc := Process{
			DisplayName:       fmt.Sprintf("%s-%s", fs.Name, c.Name),
			SystemdUnitName:   fmt.Sprintf("docker-%s-%s.service", fs.Name, c.Name),
			ProgramName:       fmt.Sprintf("%s-%s", fs.Name, c.Name),
			IsContainer:       true,
			HasRootPrivileges: c.Root,
			Image:             c.Image,
			Args:              c.Args,
			Ports:             floatschema.IntOrStrListToIntList(c.Ports),
		}
		if c.Port > 0 {
			proc.Ports = []int{int(c.Port)}
		}
		for _, port := range proc.Ports {
			containersByPort[port] = proc.DisplayName
		}
		s.Processes = append(s.Processes, proc)
	}
	for _, sd := range fs.SystemdServices {
		if strings.HasPrefix(sd, "docker-") {
			continue
		}
		proc := Process{
			DisplayName:     strings.TrimSuffix(sd, ".service"),
			ProgramName:     strings.TrimSuffix(sd, ".service"),
			SystemdUnitName: sd,
		}
		s.Processes = append(s.Processes, proc)
	}

	for _, mep := range fs.MonitoringEndpoints {
		ep := Endpoint{
			Port: mep.Port,
			Type: EndpointTypeMonitoring,
		}
		s.Endpoints = append(s.Endpoints, ep)
		seenPorts[mep.Port] = struct{}{}
	}

	for _, pep := range fs.PublicEndpoints {
		ep := Endpoint{
			Port: pep.Port,
			Type: EndpointTypePublic,
		}
		if len(pep.Domains) > 0 {
			ep.DNSNames = pep.Domains
		} else {
			ep.DNSNames = []string{
				fmt.Sprintf("%s.%s", pep.Name, publicDomain),
			}
			if pep.Sharded {
				// add shard.name.publicDomain
			}
		}
		s.Endpoints = append(s.Endpoints, ep)
		seenPorts[pep.Port] = struct{}{}
	}

	for _, port := range fs.Ports {
		if _, ok := seenPorts[int(port)]; ok {
			continue
		}
		ep := Endpoint{
			Port: int(port),
			Type: EndpointTypeInternal,
		}
		s.Endpoints = append(s.Endpoints, ep)
	}

	for i := 0; i < len(s.Endpoints); i++ {
		if c, ok := containersByPort[s.Endpoints[i].Port]; ok {
			s.Endpoints[i].ContainerName = c
		}
	}

	// Add links.
	for i := 0; i < len(s.Processes); i++ {
		s.Processes[i].addLinks(publicDomain)
	}
	for i := 0; i < len(s.Endpoints); i++ {
		s.Endpoints[i].addLinks(publicDomain)
	}
	s.addLinks(publicDomain)
	s.collectSearchTags()

	return &s
}
