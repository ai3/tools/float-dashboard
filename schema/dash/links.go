package dashschema

import (
	"fmt"
	"net/url"
	"strings"
)

const (
	grafanaSystemdDashboard = "service-overview"
	grafanaServiceDashboard = "float-services"
	grafanaHTTPDashboard    = "http"
)

var grafanaDashboards = map[string]string{
	grafanaHTTPDashboard:    "xCSUMFnmz",
	grafanaSystemdDashboard: "xfV2rd7ik",
	grafanaServiceDashboard: "9hSIJeEWz",
}

func grafanaURL(publicDomain, dashboard string, vars map[string]string) string {
	dashboardID := grafanaDashboards[dashboard]
	vals := make(url.Values)
	vals.Set("orgId", "1")
	for k, v := range vars {
		vals.Set(k, v)
	}
	return fmt.Sprintf(
		"https://grafana.%s/d/%s/%s?%s",
		publicDomain,
		dashboardID,
		dashboard,
		vals.Encode(),
	)
}

func genericLogsLink(name, publicDomain, query string) Link {
	uri := fmt.Sprintf("https://logs.%s/app/discover#/?_g=(filters:!(),refreshInterval:(pause:!t,value:0),time:(from:now-24h,to:now))&_a=(columns:!(_source),filters:!(),index:ec2f3610-55c7-11e8-823a-73397be2582f,interval:auto,query:(language:kuery,query:'%s'),sort:!())", publicDomain, query)
	return Link{
		Name: name,
		Rel:  LinkRelLogs,
		URL:  uri,
	}
}

func httpLogsLink(publicDomain, vhost string) Link {
	uri := fmt.Sprintf("https://logs.%s/app/dashboards#/view/AV9wDlJSJ4s36xPImL8m?_g=(filters:!(),refreshInterval:(pause:!t,value:0),time:(from:now-24h,to:now))&_a=(description:'',filters:!(('$state':(store:appState),meta:(alias:!n,disabled:!f,index:da92a2d0-55fd-11e8-b43b-036ee7010221,key:vhost,negate:!f,params:(query:%s),type:phrase),query:(match_phrase:(vhost:%s)))),fullScreenMode:!f,options:(darkTheme:!f,useMargins:!f),query:(language:lucene,query:''),timeRestore:!f,title:'web:%%20overview',viewMode:view)", publicDomain, vhost, vhost)
	return Link{
		Name: "logs",
		Rel:  LinkRelLogs,
		URL:  uri,
	}
}

func grafanaServiceLink(publicDomain, service string) Link {
	return Link{
		Name: "performance",
		Rel:  LinkRelDashboard,
		URL: grafanaURL(publicDomain, grafanaServiceDashboard, map[string]string{
			"var-service": service,
			"var-vhosts":  "All",
			"var-unit":    "All",
		}),
	}
}

func grafanaSystemdLink(publicDomain, systemdService string) Link {
	return Link{
		Name: "performance",
		Rel:  LinkRelDashboard,
		URL: grafanaURL(publicDomain, grafanaSystemdDashboard, map[string]string{
			"var-service": strings.TrimSuffix(systemdService, ".service"),
		}),
	}
}

func grafanaQPSLink(publicDomain, vhost string) Link {
	return Link{
		Name: "traffic",
		Rel:  LinkRelDashboard,
		URL: grafanaURL(publicDomain, grafanaHTTPDashboard, map[string]string{
			"var-vhost": fmt.Sprintf("%s:443", vhost),
		}),
	}
}

func serviceAlertsLink(publicDomain, service string) Link {
	vals := make(url.Values)
	vals.Set("q", "state=active")
	vals.Set("q", "float_service="+service)
	uri := fmt.Sprintf("https://alerts.%s/?%s", publicDomain, vals.Encode())
	return Link{
		Name: "alerts",
		Rel:  LinkRelOther,
		URL:  uri,
	}
}

func selfLink(vhost string) Link {
	return Link{
		Name: "visit site",
		Rel:  LinkRelSelf,
		URL:  "https://" + vhost,
	}
}

func (s *Service) addLinks(publicDomain string) {
	s.Links = []Link{
		grafanaServiceLink(publicDomain, s.Name),
		serviceAlertsLink(publicDomain, s.Name),
	}
}

func (p *Process) addLinks(publicDomain string) {
	p.Links = []Link{
		grafanaSystemdLink(publicDomain, p.SystemdUnitName),
		genericLogsLink("logs", publicDomain,
			fmt.Sprintf("program:\"%s\"", p.ProgramName)),
	}
}

func (e *Endpoint) addLinks(publicDomain string) {
	switch e.Type {
	case EndpointTypePublic:
		e.Links = []Link{
			selfLink(e.DNSNames[0]),
			grafanaQPSLink(publicDomain, e.DNSNames[0]),
			httpLogsLink(publicDomain, e.DNSNames[0]),
		}
	}
}
