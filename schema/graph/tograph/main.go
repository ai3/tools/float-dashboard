package main

import (
	"flag"
	"log"
	"os"
	"os/exec"
	"strings"

	floatschema "git.autistici.org/ai3/tools/float-dashboard/schema/float"
	"git.autistici.org/ai3/tools/float-dashboard/schema/graph"
	"gopkg.in/yaml.v3"
)

var service = flag.String("service", "", "filter this service only")

func main() {
	log.SetFlags(0)
	flag.Parse()

	data, err := os.ReadFile(flag.Arg(0))
	if err != nil {
		log.Fatal(err)
	}

	var svcmap map[string]*floatschema.Service
	if err := yaml.Unmarshal(data, &svcmap); err != nil {
		log.Fatal(err)
	}

	var dot string

	g := graph.ServiceGraph(svcmap)
	if *service != "" {
		g = g.Filter(graph.FilterEdgeByService(*service))
		dot = g.Render(
			graph.StyleNodeByService(*service),
			graph.StyleEdgeByService(*service),
		)
	} else {
		dot = g.Render(nil, nil)
	}

	// Run graphviz to render to SVG.
	cmd := exec.Command("dot", "-Tsvg")
	cmd.Stdin = strings.NewReader(dot)
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stdout
	if err := cmd.Run(); err != nil {
		log.Fatal(err)
	}
}
