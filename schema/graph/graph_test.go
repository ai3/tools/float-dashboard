package graph

import (
	"testing"

	floatschema "git.autistici.org/ai3/tools/float-dashboard/schema/float"
	"gopkg.in/yaml.v3"
)

const testYAML = `---
reports-collector:
  scheduling_group: frontend
  containers:
    - name: http
      image: registry.git.autistici.org/ai3/tools/reports-collector:master
      ports:
        - 3995
        - 3996
      env:
        ADDR: ":3995"
        SMTP_ADDR: ":3996"
      volumes:
        - /var/lib/GeoIP: /var/lib/GeoIP
  public_endpoints:
    - name: live-reports
      port: 3995
      scheme: http
  monitoring_endpoints:
    - job_name: reports-collector
      port: 3995
      scheme: http
  ports:
    - 3996

log-collector:
  scheduling_group: backend
  num_instances: 1
  service_credentials:
    - name: log-collector
      enable_client: false
  monitoring_endpoints:
    - job_name: rsyslog-collector
      port: 9105
      scheme: http
    - job_name: elasticsearch
      port: 9201
      scheme: http
  public_endpoints:
    - name: logs
      port: 5601
      scheme: http
      enable_sso_proxy: true
  containers:
    - name: rsyslog
      image: registry.git.autistici.org/ai3/docker/rsyslog:master
      ports:
        - 6514
        - 9105
      volumes:
        - /etc/rsyslog-collector.conf: /etc/rsyslog.conf
        - /etc/rsyslog-collector: /etc/rsyslog-collector
        - /etc/rsyslog-collector-lognorm: /etc/rsyslog-collector-lognorm
        - /var/spool/rsyslog-collector: /var/spool/rsyslog
        - /var/log/remote: /var/log/remote
    - name: kibana
      image: registry.git.autistici.org/ai3/docker/kibana:master
      port: 5601
      volumes:
        - /etc/kibana: /etc/kibana
        - /var/lib/kibana: /var/lib/kibana
      env:
        BABEL_CACHE_PATH: "/var/lib/kibana/.babelcache.json"
    - name: elasticsearch
      image: registry.git.autistici.org/ai3/docker/elasticsearch:master
      port: 9200
      volumes:
        - /etc/elasticsearch: /etc/elasticsearch
        - /var/lib/elasticsearch: /var/lib/elasticsearch
        - /var/log/elasticsearch: /var/log/elasticsearch
      env:
        PORT: 9200
        EXPORTER_PORT: 9201
  ports:
    - 6514
    - 9200
  volumes:
    - name: elasticsearch
      path: /var/lib/elasticsearch
      size: 100g
      owner: docker-log-collector
      group: docker-log-collector
      mode: "0700"
  annotations:
    dependencies:
      - client: kibana
        server: elasticsearch
      - client: log-collector-e2e/prober
        server: elasticsearch

log-collector-e2e:
  scheduling_group: all
  containers:
    - name: prober
      image: registry.git.autistici.org/ai3/tools/dye-injector:master
      port: 7094
      env:
        ADDR: ":7094"
  monitoring_endpoints:
    - name: log-collector-e2e-prober
      port: 7094
      scheme: http

`

func TestGraph(t *testing.T) {
	var svcmap map[string]*floatschema.Service
	if err := yaml.Unmarshal([]byte(testYAML), &svcmap); err != nil {
		t.Fatal(err)
	}

	g := ServiceGraph(svcmap)
	t.Logf("%s", g.Render(nil, nil))

	g = g.Filter(FilterEdgeByService("log-collector"))
	t.Logf("%s", g.Render(StyleNodeByService("log-collector"), StyleEdgeByService("log-collector")))
}
