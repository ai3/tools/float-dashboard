package graph

import "strings"

var lightGrayStyle = map[string]string{
	"color": "lightgray",
}

// FilterEdgeByService returns an edge filtering function that selects
// all edges where at least one endpoint belongs to the given service.
func FilterEdgeByService(svcname string) func(*Edge) bool {
	return func(e *Edge) bool {
		return e.Src.Service == svcname || e.Dst.Service == svcname
	}
}

// StyleNodeByService returns a node-styling function that applies
// node-specific styles depending on whether the node belongs to the
// given service or not.
func StyleNodeByService(svcname string) func(*Node) map[string]string {
	return func(n *Node) map[string]string {
		if n.Service == svcname {
			if strings.HasPrefix(n.Name, n.Service+"/") {
				return map[string]string{
					"label": quote(n.Name[len(n.Service)+1:]),
				}
			}
			return nil
		}
		return lightGrayStyle
	}
}

// StyleEdgeByService returns an edge-styling function that applies
// specific styles depending on whether the edge refers to external
// services or not.
func StyleEdgeByService(svcname string) func(*Edge) map[string]string {
	return func(e *Edge) map[string]string {
		if e.Src.Service != svcname || e.Dst.Service != svcname {
			return lightGrayStyle
		}
		return nil
	}
}
