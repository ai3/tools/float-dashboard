package graph

import (
	"bytes"
	"fmt"
	"io"
	"strings"
)

// AttributeMap is a map of Graphviz attributes.
type AttributeMap map[string]string

// Update an AttributeMap in-place with the contents of 'other'.
func (m AttributeMap) Update(other AttributeMap) {
	if other == nil {
		return
	}
	for k, v := range other {
		m[k] = v
	}
}

func (m AttributeMap) String() string {
	if len(m) == 0 {
		return ""
	}
	var parts []string
	for k, v := range m {
		parts = append(parts, fmt.Sprintf("%s=%s", k, v))
	}
	return fmt.Sprintf(" [%s]", strings.Join(parts, ","))
}

// Node in the service dependency graph. This is generally a
// high-level entity of interest: most commonly, a process
// (containerized or not).
type Node struct {
	// Node names for containers are of the form service/container.
	// Global resources lack a service/ prefix.
	Name string

	// Service identifier is used for filtering subgraphs.
	Service string

	// Display attributes for this node. They can be extended at
	// rendering time by using style functions.
	Attrs AttributeMap

	// Optional graph rank.
	Rank string
}

// NewNode creates a new Node.
func NewNode(name, service string) *Node {
	return &Node{
		Name:    name,
		Service: service,
	}
}

func newMissingNode(name, service string) *Node {
	return &Node{
		Name:    name,
		Service: service,
		Attrs: map[string]string{
			"color": "red",
		},
	}
}

// All service dependency graphs include an "internet" node,
// representing external clients.
const INTERNET = "internet"

func newInternetNode() *Node {
	return &Node{
		Name: INTERNET,
		Rank: "min",
		Attrs: map[string]string{
			"shape":     "diamond",
			"style":     "filled",
			"color":     "lightgray",
			"fillcolor": "lightgray",
			"label":     htmlQuote("<I>Internet</I>"),
		},
	}
}

const WORLD = "🌐"

func newWebNode(name, service string) *Node {
	return &Node{
		Name:    name,
		Service: service,
		Attrs: map[string]string{
			"label": quote(WORLD + " " + name),
		},
	}
}

func (n *Node) ID() string {
	return fmt.Sprintf("flow_%s", strings.Replace(strings.Replace(n.Name, "-", "_", -1), "/", "_", -1))
}

func (n *Node) render(attrFn func(*Node) map[string]string) string {
	attrs := make(AttributeMap)

	// Add our custom node IDs, that will be referenced by the
	// Javascript code.
	attrs["id"] = quote(n.ID())

	// Always add a URL attribute to nodes, so the generated SVG
	// will have an <a> tag that we can use to hook Javascript
	// interactions.
	attrs["URL"] = quote("#")

	attrs.Update(n.Attrs)
	if attrFn != nil {
		attrs.Update(attrFn(n))
	}

	if n.Rank == "" {
		return fmt.Sprintf("  \"%s\"%s\n", n.Name, attrs.String())
	}
	return fmt.Sprintf("  {\n    rank=%s\n    \"%s\"%s\n  }\n", n.Rank, n.Name, attrs.String())
}

// Edge between two nodes.
type Edge struct {
	Src, Dst *Node
	Attrs    AttributeMap
}

func NewEdge(src, dst *Node) *Edge {
	return &Edge{Src: src, Dst: dst}
}

func (e *Edge) render(attrFn func(*Edge) map[string]string) string {
	attrs := make(AttributeMap)
	attrs.Update(e.Attrs)
	if attrFn != nil {
		attrs.Update(attrFn(e))
	}
	return fmt.Sprintf("  \"%s\" -> \"%s\"%s\n", e.Src.Name, e.Dst.Name, attrs.String())
}

// The Graph type represents a service dependency graph.
type Graph struct {
	nodes map[string]*Node
	edges []*Edge
}

func NewGraph() *Graph {
	in := newInternetNode()
	return &Graph{
		nodes: map[string]*Node{
			INTERNET: in,
		},
	}
}

// Add a new Node to the graph.
func (g *Graph) addNode(n *Node) {
	g.nodes[n.Name] = n
}

// Add an Edge between two existing nodes, identified by name.
func (g *Graph) addEdge(srcName, dstName string) {
	// Find the nodes, or create "missing" nodes on demand.
	src := g.getNode(srcName)
	if src == nil {
		name, svcname := parseTag(srcName)
		src = newMissingNode(name, svcname)
		g.addNode(src)
	}

	dst := g.getNode(dstName)
	if src == nil {
		name, svcname := parseTag(dstName)
		dst = newMissingNode(name, svcname)
		g.addNode(dst)
	}

	g.edges = append(g.edges, NewEdge(src, dst))
}

func (g *Graph) getNode(name string) *Node {
	return g.nodes[name]
}

// Filter the graph by selecting edges.
func (g *Graph) Filter(selectFn func(*Edge) bool) *Graph {
	gg := NewGraph()
	for _, e := range g.edges {
		if selectFn(e) {
			gg.edges = append(gg.edges, e)
			gg.nodes[e.Src.Name] = e.Src
			gg.nodes[e.Dst.Name] = e.Dst
		}
	}
	return gg
}

// Each applies a function to all Nodes in the graph.
func (g *Graph) Each(fn func(*Node)) {
	for _, n := range g.nodes {
		fn(n)
	}
}

// Returns the list of nodes that are referenced by the edges, so we
// can avoid rendering unlinked nodes.
func (g *Graph) referencedNodes() []*Node {
	tmp := make(map[*Node]struct{})
	for _, e := range g.edges {
		tmp[e.Src] = struct{}{}
		tmp[e.Dst] = struct{}{}
	}
	out := make([]*Node, 0, len(tmp))
	for n := range tmp {
		out = append(out, n)
	}
	return out
}

// Render the graph to DOT format.
//
// nolint: errcheck
func (g *Graph) Render(nodeAttrFn func(*Node) map[string]string, edgeAttrFn func(*Edge) map[string]string) string {
	var buf bytes.Buffer

	io.WriteString(&buf, "digraph flow {\n")
	for _, n := range g.referencedNodes() {
		io.WriteString(&buf, n.render(nodeAttrFn))
	}
	for _, e := range g.edges {
		io.WriteString(&buf, e.render(edgeAttrFn))
	}
	io.WriteString(&buf, "}\n")
	return buf.String()
}

func mkTag(name, service string) string {
	if service == "" {
		return name
	}
	return fmt.Sprintf("%s/%s", service, name)
}

func parseTag(s string) (string, string) {
	n := strings.Index(s, "/")
	if n < 0 {
		return s, ""
	}
	return s[n+1:], s[:n]
}

func quote(s string) string {
	return fmt.Sprintf("\"%s\"", strings.Replace(s, "\"", "\\\"", -1))
}

func htmlQuote(s string) string {
	return fmt.Sprintf("<%s>", s)
}
