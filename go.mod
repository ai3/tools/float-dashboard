module git.autistici.org/ai3/tools/float-dashboard

go 1.14

require (
	github.com/andybalholm/brotli v1.1.1
	github.com/coreos/go-systemd/v22 v22.5.0
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/elazarl/go-bindata-assetfs v1.0.1
	github.com/kr/pretty v0.2.0 // indirect
	github.com/lpar/gzipped/v2 v2.1.0
	gopkg.in/check.v1 v1.0.0-20190902080502-41f04d3bba15 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
	gopkg.in/yaml.v3 v3.0.1
)
