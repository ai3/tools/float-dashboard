const webpack = require('webpack');
const PROD = JSON.parse(process.env.PROD || '0');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const { PurgeCSSPlugin } = require('purgecss-webpack-plugin');

module.exports = {
    context: __dirname + '/src',
    entry: {
        dashboard: [
            './js/dashboard.js',
            './css/dashboard.css',
        ],
    },
    mode: PROD ? 'production' : 'development',
    output: {
        publicPath: './',
        path: __dirname + '/assets',
        filename: PROD ? '[name]-[contenthash].min.js' : '[name].js',
        clean: true,
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: '[name]-[contenthash].css',
        }),
        new HtmlWebpackPlugin({
            template: 'index.html',
            favicon: 'favicon.ico',
            minify: false,
            inject: false,
        }),
        new PurgeCSSPlugin({
            paths: [__dirname + '/src/index.html',
		    __dirname + '/src/templates/error.dot',
		    __dirname + '/src/templates/list.dot',
		    __dirname + '/src/templates/page.dot'
	           ],
            safelist: [/^oi/, /show/, /tooltip/],
        }),
    ],
    module: {
        rules: [
            {
                test: /\.dot$/,
                use: [{
                    loader: 'dotjs-loader',
                    options: {},
                }]
            },
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, 'css-loader'],
            },
            {
                test: /\.(ico|png|gif|jpe?g|svg|eot|otf|ttf|woff)$/,
                type: 'asset/resource',
            },
        ],
    },
};
