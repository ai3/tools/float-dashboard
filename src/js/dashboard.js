import Tooltip from 'bootstrap/js/dist/tooltip';
import Modal from 'bootstrap/js/dist/modal';
import error_template from '../templates/error.dot';
import list_template from '../templates/list.dot';
import page_template from '../templates/page.dot';

var dashboard = {
    data: {},
    templates: {
        'error-template': error_template,
        'list-template': list_template,
        'page-template': page_template,
    },
};

// Render a named template with the given context.
dashboard.render = function(tplName, data) {
    return dashboard.templates[tplName](data);
};

// Update the page contents.
dashboard.update_page = function(content) {
    document.getElementById("mainContainer").innerHTML = content;
};

// Show an error message in the main page contents.
dashboard.show_error = function(err) {
    dashboard.update_page(dashboard.render('error-template', err));
};

// Refresh the list of services. Only call if the main page template
// has been already loaded.
dashboard.update_list = function(services) {
    document.getElementById("serviceList").innerHTML = dashboard.render(
        'list-template', {services: services});

    // Since we have potentially changed the <a> elements, add again
    // all the onclick listeners.
    document.querySelectorAll('a.internal').forEach(el => {
        el.addEventListener('click', function(e) {
            var filterStr = e.target.dataset.filter;
            console.log('selected filter: ' + filterStr);
            dashboard.set_filter(filterStr);
            e.preventDefault();
        });
    });

    // Same as above for tooltips.
    var tooltipTriggerList = [].slice.call(
        document.querySelectorAll('[data-bs-toggle="tooltip"]'));
    var tooltipList = tooltipTriggerList.map(function (el) {
        return new Tooltip(el)
    });

    // Initialize the flowModal element. We have to take care of activation as well.
    var flowModal = document.getElementById('flowModal');
    var flowModalObj = new Modal(flowModal, {});
    flowModal.addEventListener('show.bs.modal', function(event) {
        // Why the currentTarget? We're doing something wrong with the click call...
        var service = event.relatedTarget.currentTarget.dataset.service;
        flowModal.querySelector('.modal-title').innerText = 'Service dependency graph: ' + service;
        dashboard.render_flow_graph(service, flowModal.querySelector('.modal-body'));
        flowModalObj.handleUpdate();
    });
    document.querySelectorAll('.flow-modal-trigger').forEach(el => {
        el.addEventListener('click', function(event) {
            flowModalObj.show(event);
        });
    });
};

// Parse a filter string and return a filtering function.
dashboard.parse_filter = function(filterStr) {
    var substringMatchAny = function(needle, values) {
        for (var i = 0; i < values.length; i++) {
            if (values[i].search(needle) >= 0) {
                return true;
            }
        }
        return false;
    };

    var exactMatchAny = function(needle, values) {
        if (!values) {
            return false;
        }
        for (var i = 0; i < values.length; i++) {
            if (values[i] == needle) {
                return true;
            }
        }
        return false;
    };

    var matchTerm = function(term, value) {
        return function(s) {
            return exactMatchAny(value, s.search_tags[term]);
        };
    };
    
    var matchSubstring = function(needle) {
        return function(s) {
            for (const tag in s.search_tags) {
                if (substringMatchAny(needle, s.search_tags[tag])) {
                    return true;
                }
            };
            return false;
        };
    };

    var parseToken = function(tok) {
        var m = tok.match(/^([^:]+):(.*)$/);
        if (m) {
            return matchTerm(m[1], m[2]);
        }
        return matchSubstring(tok);
    };

    var matchers = [];
    filterStr.split(" ").forEach(tok => {
        tok = tok.trim();
        if (tok) {
            matchers.push(parseToken(tok));
        }
    });

    // Final filter ANDs all the matchers. If there are no matchers,
    // we had an empty query, which filters no results.
    if (matchers.length == 0) {
        return function(s) {
            return true;
        }
    }
    return function(s) {
        for (var i = 0; i < matchers.length; i++) {
            if (!matchers[i](s)) {
                return false;
            }
        }
        return true;
    };
};

// Apply a filter function
dashboard.apply_filter = function(services, filterFn) {
    var selected_services = [];
    services.forEach(s => {
        if (filterFn(s)) {
            selected_services.push(s);
        }
    });
    return selected_services.sort();
};

// Load services.json, or fallback to the built-in one when in demo mode.
dashboard.load_services = function(fn) {
    fetch('/services.json', {})
        .then(response => {
            if (!response.ok) {
                throw new Error('Received a response with HTTP status ' + response.status + ' from server.');
            }
            return response.json();
        })
        .then(result => {
            fn(result);
        })
        .catch(err => {
            dashboard.show_error(err);
            //console.log('fetch failed, using default services');
            //fn(defaultServices);
        });
};

// Update the services list with a new filter.
dashboard.update_filter = function(filterStr) {
    console.log('filter: ' + filterStr);
    dashboard.update_list(
        dashboard.apply_filter(
            dashboard.data.services,
            dashboard.parse_filter(filterStr)));
};

// Set the current filter, and update the list of service accordingly.
dashboard.set_filter = function(filterStr) {
    var el = document.getElementById('filterField');
    el.value = filterStr;
    el.focus();

    // Setting the field value does not cause an 'input' event to be
    // sent, so trigger the update manually.
    dashboard.update_filter(filterStr);
};

dashboard.render_flow_graph = function(service, target) {
    fetch('/flow/?service=' + service, {})
        .then(response => {
            if (!response.ok) {
                throw new Error('Received a response with HTTP status ' + response.status + ' from server.');
            }
            return response.json();
        })
        .then(result => {
            target.innerHTML = result.svg;

            // Hook clickable links in the SVG graph.
            target.querySelectorAll('g.node').forEach(el => {
                var node_meta = result.node_meta[el.id];
                if (node_meta && node_meta.service != service) {
                    var a = el.querySelector('a');
                    a.addEventListener('click', function(event) {
                        dashboard.render_flow_graph(node_meta.service, target);
                        return false;
                    });
                }
            });
        })
        .catch(err => {
            target.innerText = err.message;
        });
};

// Initialization.
window.addEventListener('load', () => {
    console.log('app ready, loading data...');
    dashboard.load_services(function(response) {
        // Late-stage initialization.
        dashboard.update_page(dashboard.render('page-template', {}));
        
        // The contents of services.json are a dictionary, but we only
        // care about the values.
        dashboard.data.services = Object.values(response);
        dashboard.update_list(dashboard.data.services);

        // Prevent form submission.
        document.getElementById('filterForm').addEventListener('submit', function(e) {
            e.preventDefault();
        });

        // Set up the filter callback.
        document.getElementById('filterField').addEventListener('input', function(e) {
            dashboard.update_filter(e.target.value);
        });
        document.getElementById('filterField').focus();
    });
});
